#include <iostream>
#include <fstream>
#include <cstddef>
#include <iostream>
#include <string>
#include <string.h>

#include "lora_decode.hpp"

int main (int argc, char *argv[]) 
{
  if(argc < 3)
  {
    std::cout << "Usage: decode <input_file> <output_file>" << std::endl;
    return 1;
  }
  
  std::ifstream inFile (argv[1],std::ios::in|std::ios::binary|std::ios::ate);
  if (inFile.is_open())
  {
    int fileSize = inFile.tellg();
    char *memblock = new char [fileSize];
    memset(memblock, 0, fileSize);
    inFile.seekg (0, std::ios::beg);
    inFile.read (memblock, fileSize);
    inFile.close();
    int numLoraBlocks = fileSize/(LORA_BLOCK +1);
    if(fileSize % (LORA_BLOCK+1) != 0)
    {
      std::cout << "INTERNAL ERROR: FILE DIMENSION IS NOT A MULTIPLE OF LORA_BLOCK * (REED_BLOCK+REED_FEC)" << std::endl;
      return -1;
    }
    
    LoRaDecoder decoder;
	
    for(int blockIdx = 0; blockIdx < numLoraBlocks; blockIdx++)
    {
      std::cout << "Processing block " << blockIdx << std::endl;
      decoder.acceptLoraMessage((unsigned char *)&memblock[blockIdx * (LORA_BLOCK+1)]);
    }
    
    std::string outMsg = decoder.getData();
    std::ofstream outFile (argv[2],std::ios::out|std::ios::binary|std::ios::ate);
    outFile.write(outMsg.data(), outMsg.length());
    return 0;
  }
}
    
    
