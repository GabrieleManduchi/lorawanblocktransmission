
COMPILER         = -c++
OPTIMIZATION_OPT = -O3
OPTIONS          = -I. -ansi -std=c++11 -pedantic-errors -Wall -Wextra -Wno-overflow -Wno-long-long $(OPTIMIZATION_OPT)
LINKER_OPTS      = -lstdc++ -lm

BUILD_LIST+=encode
BUILD_LIST+=decode
BUILD_LIST+=corrupt

HPP_SRC+=schifra_ecc_traits.hpp
HPP_SRC+=schifra_error_processes.hpp
HPP_SRC+=schifra_galois_field.hpp
HPP_SRC+=schifra_galois_field_element.hpp
HPP_SRC+=schifra_galois_field_polynomial.hpp
HPP_SRC+=schifra_reed_solomon_block.hpp
HPP_SRC+=schifra_reed_solomon_codec_validator.hpp
HPP_SRC+=schifra_reed_solomon_decoder.hpp
HPP_SRC+=schifra_reed_solomon_encoder.hpp
HPP_SRC+=schifra_reed_solomon_file_decoder.hpp
HPP_SRC+=schifra_reed_solomon_file_encoder.hpp
HPP_SRC+=schifra_reed_solomon_product_code.hpp
HPP_SRC+=schifra_reed_solomon_speed_evaluator.hpp
HPP_SRC+=schifra_sequential_root_generator_polynomial_creator.hpp

SHA1_SRC = sha1.cpp
all: $(BUILD_LIST)

$(BUILD_LIST) : %: %.cpp $(HPP_SRC)
	$(COMPILER) $(OPTIONS) -o $@ $@.cpp $(SHA1_SRC) $(LINKER_OPTS)

clean:
	rm -f core.* *.o  *~ decode encode corrupt
