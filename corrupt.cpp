#include <iostream>
#include <fstream>
#include <cstddef>
#include <iostream>
#include <string>
#include <string.h>

#include "lora_decode.hpp"

int main (int argc, char *argv[]) 
{

  if(argc < 3)
  {
    std::cout << "Usage: corrupt <input_file> <output_file>" << std::endl;
    return 1;
  }
  std::ifstream inFile (argv[1],std::ios::in|std::ios::binary|std::ios::ate);
  std::ofstream outFile (argv[2],std::ios::out|std::ios::binary|std::ios::ate);
  if (inFile.is_open())
  {
    int fileSize = inFile.tellg();
    char *memblock = new char [fileSize];
    inFile.seekg (0, std::ios::beg);
    inFile.read (memblock, fileSize);
    inFile.close();

    int numBlocks = fileSize/(LORA_BLOCK+1);
    for(int i = 0; i < numBlocks; i++)
    {
	if(i%10 != 0) //Drop a block every 10
	{
	  outFile.write(&memblock[i * (LORA_BLOCK+1)], LORA_BLOCK+1);    
	}
    }
    outFile.close();
  }
}
