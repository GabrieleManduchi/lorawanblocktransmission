#include <iostream>
#include <fstream>
#include <cstddef>
#include <iostream>
#include <string>
#include <string.h>

#include "schifra_galois_field.hpp"
#include "schifra_galois_field_polynomial.hpp"
#include "schifra_sequential_root_generator_polynomial_creator.hpp"
#include "schifra_reed_solomon_encoder.hpp"
#include "schifra_reed_solomon_decoder.hpp"
#include "schifra_reed_solomon_block.hpp"
#include "schifra_error_processes.hpp"

#include "lora_decode.hpp"
/*
#define LORA_BLOCK 50
#define REED_BLOCK 223
#define REED_FEC 32
*/
int main (int argc, char *argv[]) 
{
  if(argc < 3)
  {
    std::cout << "Usage: encode <input_file> <output_file>" << std::endl;
    return 1;
  }
   /* Finite Field Parameters */
  const std::size_t field_descriptor                =   8;
  const std::size_t generator_polynomial_index      = 120;
  const std::size_t generator_polynomial_root_count =  32;

   /* Reed Solomon Code Parameters */
  const std::size_t code_length = 255;
  const std::size_t fec_length  =  32;
  const std::size_t data_length = code_length - fec_length;

   /* Instantiate Finite Field and Generator Polynomials */
  const schifra::galois::field field(field_descriptor,
                                      schifra::galois::primitive_polynomial_size06,
                                      schifra::galois::primitive_polynomial06);

  schifra::galois::field_polynomial generator_polynomial(field);

  
  
  uint32_t fileSize;
  char * memblock;

  std::ifstream inFile (argv[1],std::ios::in|std::ios::binary|std::ios::ate);
  if (inFile.is_open())
  {
    fileSize = inFile.tellg();
    int blockSize = fileSize;
    std::cout << "Input dimension: " << blockSize << std::endl;
    //Append on head the length of the original file and on the tail its SHA1 digest
    blockSize += sizeof(uint32_t) + SHA1_STR_LEN ;
    //Make its length a multiple of the superblock length
    if(blockSize%(LORA_BLOCK * REED_BLOCK) > 0)
      blockSize += ((LORA_BLOCK * REED_BLOCK) - blockSize%(LORA_BLOCK * REED_BLOCK));
    std::cout << "Output dimension: " << blockSize << std::endl;
    memblock = new char [blockSize];
    memset(memblock, 0, blockSize);
    inFile.seekg (0, std::ios::beg);
    inFile.read (&memblock[sizeof(uint32_t)], fileSize);
    inFile.close();
    *(uint32_t *)memblock = htonl(fileSize);  //converto to networkendianity
    std::string inStr(memblock, fileSize + sizeof(uint32_t));
    SHA1 sha1;
    sha1.update(inStr);
    std::string checksum = sha1.final();
    memcpy(&memblock[fileSize + sizeof(uint32_t)], checksum.c_str(), SHA1_STR_LEN);

    if (
        !schifra::make_sequential_root_generator_polynomial(
                     field,
                     generator_polynomial_index,
                     generator_polynomial_root_count,
                     generator_polynomial)
      )
    {
      std::cout << "Error - Failed to create sequential root generator!" << std::endl;
      return 1;
    }

   /* Instantiate Encoder and Decoder (Codec) */
    typedef schifra::reed_solomon::encoder<code_length,fec_length,data_length> encoder_t;
    typedef schifra::reed_solomon::decoder<code_length,fec_length,data_length> decoder_t;

    const encoder_t encoder(field,generator_polynomial);
    const decoder_t decoder(field,generator_polynomial_index);
    
    
   std::ofstream outFile (argv[2],std::ios::out|std::ios::binary|std::ios::ate);
     
    
    
    char *buffers[LORA_BLOCK];
    for(int i = 0; i < LORA_BLOCK; i++)
      buffers[i] = new char[REED_BLOCK+REED_FEC];
    
    int numSteps = blockSize/(LORA_BLOCK * REED_BLOCK);
    for(int step = 0; step < numSteps; step++)
    {
      std::cout << "Processing super block " << step << std::endl;

      for(int i = 0;  i < LORA_BLOCK; i++)
      {
	  /* Instantiate RS Block For Codec */
	  schifra::reed_solomon::block<code_length,fec_length> block;
	  std::string currBufStr(&memblock[step * LORA_BLOCK*REED_BLOCK + i * REED_BLOCK], REED_BLOCK);
	  currBufStr.resize(REED_BLOCK+REED_FEC,0x00);
	    
	  /* Transform message into Reed-Solomon encoded codeword */
	  if (!encoder.encode(currBufStr,block))
	  {
	      std::cout << "Error - Critical decoding failure! "
			<< "Msg: " << block.error_as_string()  << std::endl;
	      return 1;
	  }
	  std::string encodedDataStr(REED_BLOCK, 0);
	  std::string encodedFecStr(REED_FEC, 0);
	  block.data_to_string(encodedDataStr);
	  block.fec_to_string(encodedFecStr);
	  encodedDataStr.copy(buffers[i], REED_BLOCK, 0);
	  encodedFecStr.copy(buffers[i]+REED_BLOCK, REED_FEC, 0);
      }
    //Buffers contain the encoded chunks of data forming the input file
      for(int bitIdx = 0; bitIdx < REED_BLOCK + REED_FEC; bitIdx++)
      {
   	 unsigned char bitIdxByte = bitIdx; //from 0 to 255
         outFile.write((const char *)&bitIdxByte, 1);     //51 bytes LoRaWAN block
 	 for(int blockIdx = 0; blockIdx < LORA_BLOCK; blockIdx++)
	   outFile.write((const char *)&buffers[blockIdx][bitIdx], 1);
      }
    }
    outFile.close();
    return 0;
  }
}

 