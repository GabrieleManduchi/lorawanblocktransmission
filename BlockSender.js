var mqtt = require('mqtt');
var fs = require('fs');
const { exec } = require('child_process');
var blockSize = 43;
var headerSize = 8;
var mqttClient;
exports.sendFile = function(filename, topic)  {
//var client  = mqtt.connect('mqtt://test.mosquitto.org')
  var mqttClient= mqtt.connect('mqtt:localhost');
  mqttClient.on('connect', function () {
    exec('encode '+filename+'  '+filename+'_encoded', (err, stdout, stderr) => {
	if (err) {
	    console.log('Could not encode the file');
	    return;
	}
	sendBytes(filename+'_encoded', topic);
      }); 
  });
}

function sendBytes(filename, topic)
{
    fs.open(filename, 'r', function(status, fd) {
      if (status) {
	  console.log('Cannot read encoded file: ' + status.message);
	  return;
      }
      const stats = fs.statSync(filename)
      const fileSizeInBytes = stats.size;
      if(fileSizeInBytes % blockSize != 0)
      {
	  console.log('Internal Error: encoded file has a size that is not a multiple of the block size(43)');
	  return;
      }
      var buf = Buffer.alloc(blockSize+headerSize);
      buf.writeInt8(0, 0); 		//Protocol version
      buf.writeInt8(100, 1) 		//Device class
      buf.writeInt8(0, 2);  		//flags
      buf.writeInt8(blockSize, 4) 	//PayloadLen
      buf.writeInt32LE(0, 4); 		//UUID
      
      var numBlocks = fileSizeInBytes/blockSize;
      for (var idx = 0; idx < numBlocks; idx++)
      {
	  fs.read(fd, buf, 8, blockSize, idx * blockSize, function(err, num, buf) {
	    console.log('Cycle: '+idx+'  read: ' + num);
	  });
      }
    });
}

