#ifndef DECODER_H
#define DECODER_H
#include <cstddef>
#include <iostream>
#include <string>
#include <arpa/inet.h>

#include "schifra_galois_field.hpp"
#include "schifra_galois_field_polynomial.hpp"
#include "schifra_sequential_root_generator_polynomial_creator.hpp"
#include "schifra_reed_solomon_encoder.hpp"
#include "schifra_reed_solomon_decoder.hpp"
#include "schifra_reed_solomon_block.hpp"
#include "schifra_error_processes.hpp"
#include "sha1.hpp"
//#define LORA_MESSAGE 51
#define LORA_MESSAGE 43  //In order to make room for the message header (8 bytes)
#define LORA_BLOCK (LORA_MESSAGE-1)
#define REED_BLOCK 223
#define REED_FEC 32
#define INVALID_IDX 10000

#define SHA1_STR_LEN 40 //ASCII representation of 20 bytes SHA1 digest

static     const std::size_t field_descriptor                =   8;
static     const std::size_t generator_polynomial_index      = 120;
static     const std::size_t generator_polynomial_root_count =  32;

   /* Reed Solomon Code Parameters */
static     const std::size_t code_length = 255;
static     const std::size_t fec_length  =  32;
static     const std::size_t data_length = code_length - fec_length;
   /* Instantiate Finite Field and Generator Polynomials */
 static    const schifra::galois::field field(field_descriptor,
                                      schifra::galois::primitive_polynomial_size06,
                                      schifra::galois::primitive_polynomial06);

class LoRaDecoder
{
  std::string outBuffer;
  std::vector <size_t> missingIdxs;
  std::string incomingBuffers[LORA_BLOCK];
  uint32_t fileSize;
   /* Finite Field Parameters */
 typedef schifra::reed_solomon::decoder<code_length,fec_length,data_length> decoder_t;
 
  int lastByteIdx;
  decoder_t *decoder;
  bool decodeOk;
  
public:
  LoRaDecoder()
  {
       schifra::galois::field_polynomial generator_polynomial(field);

       if (
        !schifra::make_sequential_root_generator_polynomial(
                     field,
                     generator_polynomial_index,
                     generator_polynomial_root_count,
                     generator_polynomial)
        )
      {
         std::cout << "Error - Failed to create sequential root generator!" << std::endl;
         return;
      }
      decoder = new decoder_t(field,generator_polynomial_index);
      for(int i = 0; i < LORA_BLOCK; i++)
      {
	incomingBuffers[i].assign(REED_BLOCK+REED_FEC, -1);
      }
      lastByteIdx = INVALID_IDX;
      decodeOk = true;
  }
  
  
  int processBlocks()
  {
//Decode and save the acquired LORA_BLOCK blocks getting REED_BLOCK bytes
    for(int blockIdx = 0; blockIdx < LORA_BLOCK; blockIdx++)
    {
      std::string dataBuffer = incomingBuffers[blockIdx].substr(0, REED_BLOCK);
      std::string fecBuffer = incomingBuffers[blockIdx].substr(REED_BLOCK, REED_FEC);
      schifra::reed_solomon::block<code_length,fec_length> block(dataBuffer, fecBuffer);
      if(missingIdxs.size() > REED_FEC) //Cannot rebuild
      {
	std::cout << "Error - too many lost messages: " << missingIdxs.size() << std::endl;
	return -1;
      }
      if (!decoder->decode(block, missingIdxs))
      //if (!decoder->decode(block))
      {
	std::cout << "Error - Critical decoding failure!" << std::endl;
	return -1;
      }
      std::string outMessage(REED_BLOCK, 0);
      block.data_to_string(outMessage);
      outBuffer.append(outMessage);
    }
    return 0;
  }

  
  
  //LoRa message dimension: LORA_BLOCK + 1
  int  acceptLoraMessage(unsigned char *msg)
  {
      unsigned char byteIdx = *msg;
      if(byteIdx < lastByteIdx && lastByteIdx != INVALID_IDX) //If a superblock has been filled
      {
	if(processBlocks())
	  decodeOk = false;
	missingIdxs.clear();
	lastByteIdx = -1;
      }
      for(size_t missingIdx = lastByteIdx +1; missingIdx < byteIdx; missingIdx++)
      {
	missingIdxs.push_back(missingIdx);
      }
      lastByteIdx = byteIdx;
      for(int blockIdx = 0; blockIdx < LORA_BLOCK; blockIdx++)
      {
	incomingBuffers[blockIdx][byteIdx] = msg[1+blockIdx];
      }
      return 0;
  }
     
     
  std::string getData()
  {
      if(lastByteIdx != INVALID_IDX && lastByteIdx != -1)
      {
	if(processBlocks())
	  decodeOk = false;
      }
      if(isDecodeOk())
	  return outBuffer.substr(sizeof(uint32_t), fileSize);
      return "";
  }
  
  bool checkSha1()
  {
      fileSize = ntohl(*(uint32_t *)outBuffer.c_str());
      SHA1 sha1;
      sha1.update(outBuffer.substr(0, fileSize+sizeof(uint32_t)));
      std::string checksum = sha1.final();
      std::cout << "Original size: " <<  fileSize << std::endl;
      std::cout << "Transmitted size: " << outBuffer.length() << std::endl;
      std::cout << "Sent SHA1 checksum: " << outBuffer.substr(sizeof(uint32_t) + fileSize, SHA1_STR_LEN) << std::endl;
      std::cout << "Computed SHA1 checksum: " << checksum << std::endl;
      
      if(outBuffer.substr(sizeof(uint32_t) + fileSize, SHA1_STR_LEN) != checksum)
      {
	std::cout << "CHECHSUM ERROR! " << std::endl;
	return false;
      }
      return true;
  }
  
  bool isDecodeOk()
  {
    std::cout << "isDecodeOk: " << decodeOk << std::endl;
    return decodeOk && checkSha1();
  }
};
	  


#endif